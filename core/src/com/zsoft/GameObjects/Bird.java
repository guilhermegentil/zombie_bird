package com.zsoft.GameObjects;

import com.badlogic.gdx.math.Vector2;

public class Bird {

	private Vector2 position;
	private Vector2 velocity;
	private Vector2 acceleratoin;
	
	private float rotation;
	private float width;
	private float height;
	
	public Bird(float x, float y, int width, int height){
		this.width = width;
		this.height = height;
		position = new Vector2(x, y);
		velocity = new Vector2(0,0);
		acceleratoin = new Vector2(0,460);
	}
	
	public float getX(){
		return position.x;
	}
	
	public float getY(){
		return position.y;
	}
	
	public float getWidth(){
		return width;
	}
	
	public float getHeight(){
		return height;
	}
	
	public float getRotation(){
		return rotation;
	}
	
	/**
	 * Set a new velocity every time the 
	 * screen is tapped.
	 */
	public void onClick(){
		velocity.y = -140;
	}
	
	public void update(float delta){
		
		velocity.add(acceleratoin.cpy().scl(delta));
		
		// Set a maximum terminal velocity
		if (velocity.y > 200)			
			velocity.y = 200;
		
		position.add(velocity.cpy().scl(delta));
		
		// Rotate counterclockwise
		if (velocity.y < 0){
			rotation -= 600 * delta;
			
			// Set the minimum rotation
			if (rotation <= -20){
				rotation = -20;
			}
		}
		
		// Rotate clockwise
		if(isFalling()){
			rotation += 480 * delta;
			
			// Set the maximum rotation
			if (rotation >= 90){
				rotation = 90;
			}
		}
	}
	
	//temp
	public boolean isFalling(){
		return velocity.y > 110;
	}
	//temp
	public boolean shouldNotFlap(){
		return velocity.y > 70;
	}
}
