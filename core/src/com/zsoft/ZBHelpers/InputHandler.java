package com.zsoft.ZBHelpers;

import com.badlogic.gdx.InputProcessor;
import com.zsoft.GameObjects.Bird;

public class InputHandler implements InputProcessor{
	
	private Bird bird;
	
	public InputHandler(Bird bird){
		// A reference to gameWorld's bird
		this.bird = bird;
	}
	
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		bird.onClick();
		
		// Return true to say we handled the touch successfully
		return true;
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
