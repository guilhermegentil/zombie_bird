package com.zsoft.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.zsoft.GameWorld.GameRenderer;
import com.zsoft.GameWorld.GameWorld;
import com.zsoft.ZBHelpers.InputHandler;

public class GameScreen implements Screen{
	
	private GameWorld world;
	private GameRenderer renderer;
	
	private float runTime = 0f;
	
	public GameScreen(){
		System.out.println("GameScreen Created!");
		
		float screenWidth = Gdx.graphics.getWidth();
		float screenHeight = Gdx.graphics.getHeight();
		float gameWidth = 136;
		float gameHeight = screenHeight / (screenWidth / gameWidth);
		
		int midPointY = (int) (gameHeight / 2);
		
		world = new GameWorld(midPointY);
		renderer = new GameRenderer(world, (int)gameHeight, midPointY);
		
		Gdx.input.setInputProcessor(new InputHandler(world.getBird()));
	}

	@Override
	public void render(float delta) {
		runTime += delta;
		
		// We are passing in delta to our update method 
		// so that we can perform frame-rate independent movement. 
		world.update(delta);
		renderer.render(runTime);
	}

	@Override
	public void resize(int width, int height) {
		System.out.println("GameScreen - resize");		
	}

	@Override
	public void show() {
		System.out.println("GameScreen - show");
	}

	@Override
	public void hide() {
		System.out.println("GameScreen - hide");
	}

	@Override
	public void pause() {
		System.out.println("GameScreen - pause");
	}

	@Override
	public void resume() {
		System.out.println("GameScreen - resume");
	}

	@Override
	public void dispose() {
		System.out.println("GameScreen - dispose");
	}

}
